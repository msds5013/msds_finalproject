import csv, sqlite3

conn = sqlite3.connect( "mycsv.db" )
conn.text_factory = str  #bugger 8-bit bytestrings
cur = conn.cursor()
cur.execute('CREATE TABLE IF NOT EXISTS mytable (field1 VARCHAR, field2 VARCHAR, field3 VARCHAR)')


reader = csv.reader(open("planet-1-data-2.csv", "rb"))

for field1, field2, field3 in reader:

	cur.execute("INSERT INTO mytable(field1,field2,field3) VALUES (?,?,?)", (field1, field2, field3))
	print field1,field2,field3

conn.commit()



