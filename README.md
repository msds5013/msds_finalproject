This is the final project for MSDS-5013

The importDataFromCSV.py will import the csv data file.

The importData.py will import the csv data file, and will separate the into three separate tables.
The three tables are RADAN, BORAN, and PATIENT. 
The BORAN and RADAN schema are as follow 'patient_id, bloodpressure, exercise, weight, glucose, bmix, planet_id'.

You would need to supply the datafile from command line. 
For example `python importDataFromCSV.py radan.csv`. 
