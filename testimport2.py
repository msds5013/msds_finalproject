import csv, sqlite3

conn = sqlite3.connect( "finalProject.db" )
conn.text_factory = str  #bugger 8-bit bytestrings
cur = conn.cursor()
cur.execute('''CREATE TABLE IF NOT EXISTS ALIEN_PATIENT_DATA (DEIDENTIFY VARCHAR
                , BLOODPRESSURE VARCHAR, EXERCISE VARCHAR, WEIGHT VARCHAR
                , GLUCOSE VARCHAR, BMIX VARCHAR, PLANET_ID VARCHAR)''')


reader = csv.reader(open("radan.csv", "rb"))

for deidentify, bloodpressure, exercise, weight, glucose, bmix, planet_id in reader:

	cur.execute("""INSERT INTO ALIEN_PATIENT_DATA(deidentify, bloodpressure
                        , exercise, weight, glucose, bmix, planet_id) 
                        VALUES (?,?,?,?,?,?,?)"""
                        , (deidentify, bloodpressure
                        , exercise, weight, glucose, bmix, planet_id))
	print  deidentify, bloodpressure, exercise, weight, glucose, bmix, planet_id

conn.commit()



