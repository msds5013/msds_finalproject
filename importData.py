#!/usr/bin/env python
import csv, sqlite3, sys

inFile = sys.argv[1]

conn = sqlite3.connect( "finalProject.db" )
conn.text_factory = str  #bugger 8-bit bytestrings
cur = conn.cursor()
cur.execute('''CREATE TABLE IF NOT EXISTS BORAN (patient_id VARCHAR NOT NULL
                , BLOODPRESSURE VARCHAR, EXERCISE VARCHAR, WEIGHT VARCHAR
                , GLUCOSE VARCHAR, BMIX VARCHAR, PLANET_ID VARCHAR NOT NULL)''')
cur.execute('''CREATE TABLE IF NOT EXISTS RADAN (patient_id VARCHAR NOT NULL
                , BLOODPRESSURE VARCHAR, EXERCISE VARCHAR, WEIGHT VARCHAR
                , GLUCOSE VARCHAR, BMIX VARCHAR, PLANET_ID VARCHAR NOT NULL)''')
cur.execute('''CREATE TABLE IF NOT EXISTS PATIENT (patient_id VARCHAR NOT NULL, age VARCHAR)''')


reader = (csv.reader(open(inFile, "rt", newline=''), delimiter=','))
readin = (csv.reader(open(inFile, "rt", newline=''), delimiter=','))
def checkFile():
    if (reader):
        checkTableImport()
    else:
        print("There is no File. Please select a file.")


def checkTableImport():
    columns = len(next(reader))
    if (columns == 7):
        importAlienPatientData()
    elif (columns == 2):
        deidentifyList()
    else:
        print("""The data you've imported is in the incorrect format.
        Please verify that you only have two columns or seven columns of data.""")


def importAlienPatientData(): 
    for row in reader:
        index = row[6]
        if (index == '1'):
            importBORAN()
        elif (index == '2'):
            importRADAN()
        else:
            print("Please check the data planet id. It needs to be a 1 or a 2.")

def importRADAN():
    for patient_id, bloodpressure, exercise, weight, glucose, bmix, planet_id in readin:
        cur.execute("""INSERT INTO RADAN(patient_id, bloodpressure
                        , exercise, weight, glucose, bmix, planet_id) 
                        VALUES (?,?,?,?,?,?,?)"""
                        , (patient_id, bloodpressure
                        , exercise, weight, glucose, bmix, planet_id))
        print (patient_id, bloodpressure, exercise, weight, glucose, bmix, planet_id)
    conn.commit()

def importBORAN():
    for patient_id, bloodpressure, exercise, weight, glucose, bmix, planet_id in readin:
        cur.execute("""INSERT INTO BORAN(patient_id, bloodpressure
                        , exercise, weight, glucose, bmix, planet_id) 
                        VALUES (?,?,?,?,?,?,?)"""
                        , (patient_id, bloodpressure
                        , exercise, weight, glucose, bmix, planet_id))
        print (patient_id, bloodpressure, exercise, weight, glucose, bmix, planet_id)
    conn.commit()
    
def deidentifyList():
    for patient_id, age in readin:
        cur.execute("""INSERT INTO PATIENT(patient_id, age) VALUES (?,?)"""
                , (patient_id, age))
        print (patient_id, age)
    conn.commit()


checkFile()
